namespace Users.Core.Exceptions;

public class FetchTokenException : Exception
{
    public FetchTokenException(string message) : base(message)
    {
        // Empty
    }
}