﻿CREATE TABLE IF NOT EXISTS "__EFMigrationsHistory" (
    "MigrationId" character varying(150) NOT NULL,
    "ProductVersion" character varying(32) NOT NULL,
    CONSTRAINT "PK___EFMigrationsHistory" PRIMARY KEY ("MigrationId")
);

START TRANSACTION;

CREATE TABLE "Funds" (
    "Id" uuid NOT NULL,
    "Name" text NOT NULL,
    "Currency" integer NOT NULL,
    CONSTRAINT "PK_Funds" PRIMARY KEY ("Id")
);

CREATE TABLE "Bonds" (
    "FundId" uuid NOT NULL,
    "Yield" real NOT NULL,
    "Price" double precision NOT NULL,
    CONSTRAINT "PK_Bonds" PRIMARY KEY ("FundId"),
    CONSTRAINT "FK_Bonds_Funds_FundId" FOREIGN KEY ("FundId") REFERENCES "Funds" ("Id") ON DELETE CASCADE
);

CREATE TABLE "Equities" (
    "FundId" uuid NOT NULL,
    "Sector" integer NOT NULL,
    "Exchange" integer NOT NULL,
    "Value" double precision NOT NULL,
    CONSTRAINT "PK_Equities" PRIMARY KEY ("FundId"),
    CONSTRAINT "FK_Equities_Funds_FundId" FOREIGN KEY ("FundId") REFERENCES "Funds" ("Id") ON DELETE CASCADE
);

CREATE TABLE "ExchangeTradedFunds" (
    "FundId" uuid NOT NULL,
    "RiskRating" smallint NOT NULL,
    "Price" double precision NOT NULL,
    "DistributionStatus" integer NOT NULL,
    CONSTRAINT "PK_ExchangeTradedFunds" PRIMARY KEY ("FundId"),
    CONSTRAINT "FK_ExchangeTradedFunds_Funds_FundId" FOREIGN KEY ("FundId") REFERENCES "Funds" ("Id") ON DELETE CASCADE
);

CREATE TABLE "MutualFunds" (
    "FundId" uuid NOT NULL,
    "RiskRating" smallint NOT NULL,
    "NetAssetValue" double precision NOT NULL,
    "DistributionStatus" integer NOT NULL,
    CONSTRAINT "PK_MutualFunds" PRIMARY KEY ("FundId"),
    CONSTRAINT "FK_MutualFunds_Funds_FundId" FOREIGN KEY ("FundId") REFERENCES "Funds" ("Id") ON DELETE CASCADE
);

INSERT INTO "__EFMigrationsHistory" ("MigrationId", "ProductVersion")
VALUES ('20231025221540_Add initial', '7.0.11');

COMMIT;

START TRANSACTION;

INSERT INTO "Funds" ("Id", "Name", "Currency")
VALUES ('bf123c7a-807d-48b7-9e76-2c4326f6379a', '3M CO', 2);

INSERT INTO "Equities" ("FundId", "Sector", "Exchange", "Value")
VALUES ('bf123c7a-807d-48b7-9e76-2c4326f6379a', 8, 1, 89.390000000000001);

INSERT INTO "Funds" ("Id", "Name", "Currency")
VALUES ('1aceecc9-f967-4984-bdc1-ee6f937cbd56', 'Microsoft CORP', 2);

INSERT INTO "Equities" ("FundId", "Sector", "Exchange", "Value")
VALUES ('1aceecc9-f967-4984-bdc1-ee6f937cbd56', 7, 0, 340.67000000000002);

INSERT INTO "Funds" ("Id", "Name", "Currency")
VALUES ('16381e33-d5af-48da-aa07-c4c809259b7a', 'CBC PLC 4.4 07/07/2027', 1);

INSERT INTO "Bonds" ("FundId", "Yield", "Price")
VALUES ('16381e33-d5af-48da-aa07-c4c809259b7a', 4.32, 100.25);

INSERT INTO "Funds" ("Id", "Name", "Currency")
VALUES ('97668dd4-6a26-4a81-a9f8-c1ed4b4e7dea', 'DBR 1 15/08/25', 1);

INSERT INTO "Bonds" ("FundId", "Yield", "Price")
VALUES ('97668dd4-6a26-4a81-a9f8-c1ed4b4e7dea', 3.18, 96.239999999999995);

INSERT INTO "Funds" ("Id", "Name", "Currency")
VALUES ('0926b072-31a3-41ab-92c8-bea2318343b7', 'GLOBAL X HYDROGEN UCITS ETF USD ACC', 1);

INSERT INTO "ExchangeTradedFunds" ("FundId", "RiskRating", "Price", "DistributionStatus")
VALUES ('0926b072-31a3-41ab-92c8-bea2318343b7', 7, 6.0800000000000001, 0);

INSERT INTO "Funds" ("Id", "Name", "Currency")
VALUES ('4cfe340a-9019-47d4-97ff-b5833a2cacb1', 'GLOBAL X CYBERSECURITY UCITS ETF USD ACC', 1);

INSERT INTO "ExchangeTradedFunds" ("FundId", "RiskRating", "Price", "DistributionStatus")
VALUES ('4cfe340a-9019-47d4-97ff-b5833a2cacb1', 7, 9.7699999999999996, 0);

INSERT INTO "Funds" ("Id", "Name", "Currency")
VALUES ('10ccfe6d-e2c1-41df-a8e3-09981a2569d7', 'AB - AMERICAN GROWTH PORTFOLIO A EUR ACC', 1);

INSERT INTO "MutualFunds" ("FundId", "RiskRating", "NetAssetValue", "DistributionStatus")
VALUES ('10ccfe6d-e2c1-41df-a8e3-09981a2569d7', 6, 150.56, 0);

INSERT INTO "Funds" ("Id", "Name", "Currency")
VALUES ('6b373916-ae6b-43bb-af79-80131dbfed7f', 'ABRDN SICAV I - SELECT EURO HIGH YIELD BOND FUND A MINC EUR', 1);

INSERT INTO "MutualFunds" ("FundId", "RiskRating", "NetAssetValue", "DistributionStatus")
VALUES ('6b373916-ae6b-43bb-af79-80131dbfed7f', 4, 5.1600000000000001, 1);

INSERT INTO "__EFMigrationsHistory" ("MigrationId", "ProductVersion")
VALUES ('20231025221608_Seed funds', '7.0.11');

COMMIT;

START TRANSACTION;

CREATE TABLE "Orders" (
    "Email" text NOT NULL,
    "FundId" uuid NOT NULL,
    "Quantity" bigint NOT NULL,
    CONSTRAINT "PK_Orders" PRIMARY KEY ("Email", "FundId"),
    CONSTRAINT "FK_Orders_Funds_FundId" FOREIGN KEY ("FundId") REFERENCES "Funds" ("Id") ON DELETE CASCADE
);

CREATE INDEX "IX_Orders_FundId" ON "Orders" ("FundId");

INSERT INTO "__EFMigrationsHistory" ("MigrationId", "ProductVersion")
VALUES ('20231026212245_Add Orders', '7.0.11');

COMMIT;

