namespace Trader.Core.Exceptions;

public abstract class TraderException : Exception
{
    protected TraderException(string message) : base(message)
    {
        // Empty
    }
}