namespace Trader.Core.Exceptions;

public class NotEnoughFundsException : TraderException
{
    public NotEnoughFundsException(string message) : base(message)
    {
        // Empty
    }
}