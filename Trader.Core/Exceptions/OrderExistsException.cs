namespace Trader.Core.Exceptions;

public class OrderExistsException : TraderException
{
    public OrderExistsException(string message) : base(message)
    {
        // Empty
    }
}