using Trader.Core.Enums;

namespace Trader.Core.Entities;

public class MutualFund : Fund
{
    public short RiskRating { get; set; }
    public double NetAssetValue { get; set; }
    public DistributionStatus DistributionStatus { get; set; }
}