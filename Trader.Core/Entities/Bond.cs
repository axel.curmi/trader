namespace Trader.Core.Entities;

public class Bond : Fund
{
    public float Yield { get; set; }
    public double Price { get; set; }
}