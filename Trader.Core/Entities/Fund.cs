using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using Trader.Core.Enums;

namespace Trader.Core.Entities;

[JsonDerivedType(typeof(Equity))]
[JsonDerivedType(typeof(Bond))]
[JsonDerivedType(typeof(ExchangeTradedFund))]
[JsonDerivedType(typeof(MutualFund))]
public abstract class Fund
{
    [Key]
    public Guid Id { get; set; }
    public string Name { get; set; } = string.Empty;
    public Currency Currency { get; set; } = Currency.Euro;
    public string Type => GetType().Name;
}