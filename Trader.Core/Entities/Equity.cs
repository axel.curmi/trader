using Trader.Core.Enums.Equity;

namespace Trader.Core.Entities;

public class Equity : Fund
{
    public Sector Sector { get; set; }
    public Exchange Exchange { get; set; }
    public double Value { get; set; }
}