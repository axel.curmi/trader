using Trader.Core.Enums;

namespace Trader.Core.Entities;

public class ExchangeTradedFund : Fund
{
    public short RiskRating { get; set; }
    public double Price { get; set; }
    public DistributionStatus DistributionStatus { get; set; }
}