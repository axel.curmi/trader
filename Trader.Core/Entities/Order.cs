using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using Microsoft.EntityFrameworkCore;

namespace Trader.Core.Entities;

[PrimaryKey(nameof(Email), nameof(FundId))]
public class Order
{
    [JsonIgnore]
    public string Email { get; set; } = string.Empty;
    [ForeignKey("FundId")]
    [JsonIgnore]
    public Guid FundId { get; set; }

    public virtual Fund Fund { get; set; }
    public uint Quantity { get; set; }
}