namespace Trader.Core.Enums;

public enum DistributionStatus
{
    Accumulating,
    Distributing
}