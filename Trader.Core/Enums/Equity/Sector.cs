namespace Trader.Core.Enums.Equity;

public enum Sector
{
    Energy,
    Materials,
    Healthcare,
    Financials,
    ConsumerDefensive,
    ConsumerCyclical,
    CommunicationServices,
    Technology,
    Industrials,
    Utilities,
    RealEstate
}