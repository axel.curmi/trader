namespace Trader.Core.Enums;

public enum Currency
{
    Euro = 1,
    UsDollar = 2,
    PoundSterling = 3
}