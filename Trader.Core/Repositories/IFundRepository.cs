using Trader.Core.Entities;

namespace Trader.Core.Repositories;

public interface IFundRepository
{
    /// <summary>
    /// Makes use of the DbContext to fetch all funds in the DB.
    /// </summary>
    /// <returns>IEnumerable containing all funds available in the DB.</returns>
    IEnumerable<Fund> GetAll();
}