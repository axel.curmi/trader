using Trader.Core.Entities;

namespace Trader.Core.Repositories;

public interface IOrderRepository
{
    /// <summary>
    /// Makes use of the DbContext to fetch all orders linked to the given email in the DB.
    /// </summary>
    /// <param name="email">The email address to fetch orders for.</param>
    /// <returns>An IEnumerable containing all orders linked the the given email.</returns>
    IEnumerable<Order> GetOrdersByEmail(string email);
    
    /// <summary>
    /// Makes use of the DbContext to fetch an order linked to the given email and fundId in the DB if exists,
    /// null otherwise.
    /// </summary>
    /// <param name="email">The email address to fetch the order for.</param>
    /// <param name="fundId">The fundId to fetch the order for.</param>
    /// <returns>An order if exists, null otherwise.</returns>
    Order? GetOrderByEmailAndFundId(string email, Guid fundId);
    
    /// <summary>
    /// Makes use of the DbContext to add an order to the DB.
    /// </summary>
    /// <param name="orderToAdd">The order to add.</param>
    /// <returns>The order added</returns>
    Order AddOrder(Order orderToAdd);
    
    /// <summary>
    /// Makes use of the DbContext to update an order in the DB.
    /// </summary>
    /// <param name="orderToUpdate">The updated order to persist.</param>
    /// <returns>The order with updated details.</returns>
    Order UpdateOrder(Order orderToUpdate);
    
    /// <summary>
    /// Makes use of the DbContext to delete an order from the DB.
    /// </summary>
    /// <param name="orderToDelete">The order to delete.</param>
    void DeleteOrder(Order orderToDelete);
}