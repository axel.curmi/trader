using Microsoft.AspNetCore.Mvc;
using Users.Application.Dtos;
using Users.Application.Services;

namespace Users.API.Controllers;

[ApiController]
[Route("/api/users")]
public class UsersController : ControllerBase
{
    private readonly ITokenService _tokenService;

    public UsersController(ITokenService tokenService)
    {
        _tokenService = tokenService;
    }
    
    [HttpPost("token")]
    public async Task<ActionResult<GetTokenResponseDto>> GetToken(GetTokenRequestDto requestDto)
    {
        var response = await _tokenService.GetToken(requestDto);
        return response switch
        {
            GetTokenSuccessfulResponseDto => Ok(response),
            GetTokenErrorResponseDto => Unauthorized(response),
            _ => BadRequest(response)
        };
    }
}