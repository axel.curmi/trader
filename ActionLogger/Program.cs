using ActionLogger.Services;
using Commons;

IHost host = Host.CreateDefaultBuilder(args)
    .ConfigureServices((hostContext, services) =>
    {
        services.AddRabbitMqMessaging(hostContext.Configuration);
        services.AddHostedService<ActionLogService>();
    })
    .Build();

host.Run();