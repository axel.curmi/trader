using System.Text;
using System.Text.Json;
using Commons.Dtos;
using Commons.Messaging;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using Constants = Commons.Constants;

namespace ActionLogger.Services;

/// <summary>
/// Background service that will be kept alive until eventually terminated.
/// A RabbitMQ consumer is created and listens for any new messages on the ActionLogQueue and logs it.
/// </summary>
public class ActionLogService : BackgroundService
{
    private readonly ILogger<ActionLogService> _logger;

    public ActionLogService(IRabbitMqClient rabbitMqClient, ILogger<ActionLogService> logger)
    {
        _logger = logger;

        var consumer = new EventingBasicConsumer(rabbitMqClient.Channel);
        consumer.Received += ActionLoggerConsume;
        rabbitMqClient.Channel.BasicConsume(
            queue: Constants.ActionLoggerQueue,
            autoAck: true,
            consumer: consumer);
    }

    private void ActionLoggerConsume(object? sender, BasicDeliverEventArgs args)
    {
        var stringBody = Encoding.UTF8.GetString(args.Body.ToArray());
        var dto = JsonSerializer.Deserialize<TradeOrderActionLogDto>(stringBody);
        if (dto == null)
        {
            _logger.LogError("Failed to deserialize the following message in {queueName} :: {message}",
                Constants.ActionLoggerQueue, stringBody);
        }
        else
        {
            _logger.LogInformation("{email} has {action} {quantity} {fund}",
                dto.Email, dto.Action == "buy" ? "bought" : "sold", dto.Quantity, dto.FundName);
        }
    }
    
    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        while (!stoppingToken.IsCancellationRequested)
        {
            await Task.Delay(1000, stoppingToken);
        }
    }
}