using Microsoft.EntityFrameworkCore;
using Trader.Core.Entities;

namespace Trader.Infrastructure.Data;

internal class TraderContext : DbContext
{
    public TraderContext(DbContextOptions<TraderContext> options) : base(options)
    {
        // Empty
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        // https://learn.microsoft.com/en-us/ef/core/modeling/table-splitting
        modelBuilder.Entity<Fund>().ToTable("Funds");

        modelBuilder.Entity<Equity>().ToTable("Equities",
            tableBuilder => tableBuilder.Property(equity => equity.Id).HasColumnName("FundId"));

        modelBuilder.Entity<Bond>().ToTable("Bonds",
            tableBuilder => tableBuilder.Property(bond => bond.Id).HasColumnName("FundId"));

        modelBuilder.Entity<ExchangeTradedFund>().ToTable("ExchangeTradedFunds",
            tableBuilder => tableBuilder.Property(etf => etf.Id).HasColumnName("FundId"));

        modelBuilder.Entity<MutualFund>().ToTable("MutualFunds",
            tableBuilder => tableBuilder.Property(mutualFund => mutualFund.Id).HasColumnName("FundId"));
    }

    public DbSet<Fund> Funds => Set<Fund>();
    public DbSet<Order> Orders => Set<Order>();
}