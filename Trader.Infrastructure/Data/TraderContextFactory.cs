using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace Trader.Infrastructure.Data;

internal class TraderContextFactory : IDesignTimeDbContextFactory<TraderContext>
{
    public TraderContext CreateDbContext(string[] args)
    {
        var optionsBuilder = new DbContextOptionsBuilder<TraderContext>();
        optionsBuilder.UseNpgsql(
            "Host=localhost;Database=trader;Username=postgres;Password=postgres");

        return new TraderContext(optionsBuilder.Options);
    }
}