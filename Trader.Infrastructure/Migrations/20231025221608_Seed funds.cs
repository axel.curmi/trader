﻿using Microsoft.EntityFrameworkCore.Migrations;
using Trader.Core.Entities;
using Trader.Core.Enums;
using Trader.Core.Enums.Equity;

#nullable disable

namespace Trader.Infrastructure.Migrations
{
    /// <inheritdoc />
    public partial class Seedfunds : Migration
    {
        private Fund[] _funds = new Fund[]
            {
                new Equity()
                {
                    Id = Guid.Parse("bf123c7a-807d-48b7-9e76-2c4326f6379a"),
                    Name = "3M CO",
                    Currency = Currency.UsDollar,
                    Sector = Sector.Industrials,
                    Exchange = Exchange.NewYorkStockExchange,
                    Value = 89.39
                },
                new Equity()
                {
                    Id = Guid.Parse("1aceecc9-f967-4984-bdc1-ee6f937cbd56"),
                    Name = "Microsoft CORP",
                    Currency = Currency.UsDollar,
                    Sector = Sector.Technology,
                    Exchange = Exchange.Nasdaq,
                    Value = 340.67
                },
                new Bond()
                {
                    Id = Guid.Parse("16381e33-d5af-48da-aa07-c4c809259b7a"),
                    Name = "CBC PLC 4.4 07/07/2027",
                    Currency = Currency.Euro,
                    Yield = 4.32f,
                    Price = 100.25
                },
                new Bond()
                {
                    Id = Guid.Parse("97668dd4-6a26-4a81-a9f8-c1ed4b4e7dea"),
                    Name = "DBR 1 15/08/25",
                    Currency = Currency.Euro,
                    Yield = 3.18f,
                    Price = 96.24
                },
                new ExchangeTradedFund()
                {
                    Id = Guid.Parse("0926b072-31a3-41ab-92c8-bea2318343b7"),
                    Name = "GLOBAL X HYDROGEN UCITS ETF USD ACC",
                    Currency = Currency.Euro,
                    RiskRating = 7,
                    Price = 6.08,
                    DistributionStatus = DistributionStatus.Accumulating
                },
                new ExchangeTradedFund()
                {
                    Id = Guid.Parse("4cfe340a-9019-47d4-97ff-b5833a2cacb1"),
                    Name = "GLOBAL X CYBERSECURITY UCITS ETF USD ACC",
                    Currency = Currency.Euro,
                    RiskRating = 7,
                    Price = 9.77,
                    DistributionStatus = DistributionStatus.Accumulating
                },
                new MutualFund()
                {
                    Id = Guid.Parse("10ccfe6d-e2c1-41df-a8e3-09981a2569d7"),
                    Name = "AB - AMERICAN GROWTH PORTFOLIO A EUR ACC",
                    Currency = Currency.Euro,
                    RiskRating = 6,
                    NetAssetValue = 150.56,
                    DistributionStatus = DistributionStatus.Accumulating
                },
                new MutualFund()
                {
                    Id = Guid.Parse("6b373916-ae6b-43bb-af79-80131dbfed7f"),
                    Name = "ABRDN SICAV I - SELECT EURO HIGH YIELD BOND FUND A MINC EUR",
                    Currency = Currency.Euro,
                    RiskRating = 4,
                    NetAssetValue = 5.16,
                    DistributionStatus = DistributionStatus.Distributing
                }
            };
        
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            foreach (var fund in _funds)
            {
                migrationBuilder.InsertData(
                    table: "Funds",
                    columns: new [] { "Id", "Name", "Currency" },
                    values: new object[] { fund.Id.ToString(), fund.Name, (int)fund.Currency });

                switch (fund)
                {
                    case Equity equity:
                    {
                        migrationBuilder.InsertData(
                            table: "Equities",
                            columns: new [] { "FundId", "Sector", "Exchange", "Value" },
                            values: new object[] { equity.Id.ToString(), (int)equity.Sector, (int)equity.Exchange, equity.Value });
                        break;
                    }
                    case Bond bond:
                    {
                        migrationBuilder.InsertData(
                            table: "Bonds",
                            columns: new [] { "FundId", "Yield", "Price" },
                            values: new object[] { bond.Id.ToString(), bond.Yield, bond.Price });
                        break;
                    }
                    case ExchangeTradedFund etf:
                    {
                        migrationBuilder.InsertData(
                            table: "ExchangeTradedFunds",
                            columns: new [] { "FundId", "RiskRating", "Price", "DistributionStatus" },
                            values: new object[] { etf.Id.ToString(), etf.RiskRating, etf.Price, (int)etf.DistributionStatus });
                        break;
                    }
                    case MutualFund mutualFund:
                    {
                        migrationBuilder.InsertData(
                            table: "MutualFunds",
                            columns: new [] { "FundId", "RiskRating", "NetAssetValue", "DistributionStatus" },
                            values: new object[] { mutualFund.Id.ToString(), mutualFund.RiskRating, mutualFund.NetAssetValue, (int)mutualFund.DistributionStatus });
                        break;
                    }
                }
            }
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            foreach (var fund in _funds)
            {
                migrationBuilder.DeleteData(
                    table: "Funds",
                    keyColumn: "Id",
                    keyValue: fund.Id);
                
                switch (fund)
                {
                    case Equity equity:
                    {
                        migrationBuilder.DeleteData(
                            table: "Equities",
                            keyColumn: "FundId",
                            keyValue: equity.Id);
                        
                        break;
                    }
                    case Bond bond:
                    {
                        migrationBuilder.DeleteData(
                            table: "Bonds",
                            keyColumn: "FundId",
                            keyValue: bond.Id);
                        break;
                    }
                    case ExchangeTradedFund etf:
                    {
                        migrationBuilder.DeleteData(
                            table: "ExchangeTradedFunds",
                            keyColumn: "FundId",
                            keyValue: etf.Id);
                        break;
                    }
                    case MutualFund mutualFund:
                    {
                        migrationBuilder.DeleteData(
                            table: "MutualFunds",
                            keyColumn: "FundId",
                            keyValue: mutualFund.Id);
                        break;
                    }
                }
            }
        }
    }
}
