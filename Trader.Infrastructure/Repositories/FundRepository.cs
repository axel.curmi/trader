using Trader.Core.Entities;
using Trader.Core.Repositories;
using Trader.Infrastructure.Data;

namespace Trader.Infrastructure.Repositories;

internal class FundRepository : IFundRepository
{
    private readonly TraderContext _context;

    public FundRepository(TraderContext context)
    {
        _context = context;
    }
    
    public IEnumerable<Fund> GetAll()
    { 
        return _context.Funds.ToList();
    }
}