using Microsoft.EntityFrameworkCore;
using Trader.Core.Entities;
using Trader.Core.Repositories;
using Trader.Infrastructure.Data;

namespace Trader.Infrastructure.Repositories;

internal class OrderRepository : IOrderRepository
{
    private readonly TraderContext _context;

    public OrderRepository(TraderContext context)
    {
        _context = context;
    }

    public IEnumerable<Order> GetOrdersByEmail(string email)
    {
        return _context.Orders.Where(order => order.Email == email)
            .Include(o => o.Fund)
            .ToArray();
    }

    public Order? GetOrderByEmailAndFundId(string email, Guid fundId)
    {
        return _context.Orders.Where(o => o.Email == email &&
                                          o.FundId == fundId)
            .Include(o => o.Fund)
            .FirstOrDefault();
    }

    public Order AddOrder(Order orderToAdd)
    {
        var order = _context.Orders.Add(orderToAdd);
        _context.SaveChanges();
        return GetOrderByEmailAndFundId(orderToAdd.Email, orderToAdd.FundId)!;
    }

    public Order UpdateOrder(Order orderToUpdate)
    {
        var order = _context.Update(orderToUpdate);
        _context.SaveChanges();
        return order.Entity;
    }

    public void DeleteOrder(Order orderToDelete)
    {
        _context.Orders.Remove(orderToDelete);
        _context.SaveChanges();
    }
}