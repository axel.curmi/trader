using System.Text.Json;
using Commons;
using Commons.Messaging;

namespace Trader.Infrastructure.Messaging;

/// <summary>
/// RabbitMqPublisher is used to publish messages via RabbitMQ messaging system.
/// </summary>
internal class RabbitMqPublisher : IPublisher
{
    private readonly IRabbitMqClient _rabbitMqClient;

    public RabbitMqPublisher(IRabbitMqClient rabbitMqClient)
    {
        _rabbitMqClient = rabbitMqClient;
    }
    
    public void Publish<T>(T value, string exchange)
    {
        _rabbitMqClient.Channel.BasicPublish(
            exchange: exchange,
            routingKey: string.Empty,
            mandatory: true,
            basicProperties: null,
            body: JsonSerializer.SerializeToUtf8Bytes(value));
    }

    public void PublishToActionLog<T>(T value)
    {
        Publish(value, Constants.ActionLoggerExchange);
    }
}