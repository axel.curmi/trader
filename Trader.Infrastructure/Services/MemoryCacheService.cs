using Microsoft.Extensions.Caching.Memory;
using Trader.Application.Services;

namespace Trader.Infrastructure.Services;

public class MemoryCacheService : ICacheService
{
    private readonly IMemoryCache _memoryCache;

    public MemoryCacheService(IMemoryCache memoryCache)
    {
        _memoryCache = memoryCache;
    }
    
    public T? Get<T>(string key)
    {
        return _memoryCache.Get<T>(key);
    }

    public void Set<T>(string key, T value, DateTimeOffset expiry)
    {
        _memoryCache.Set<T>(key, value, expiry);
    }
}