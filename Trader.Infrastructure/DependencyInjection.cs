using Commons;
using Commons.Messaging;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Trader.Application.Services;
using Trader.Core.Repositories;
using Trader.Infrastructure.Data;
using Trader.Infrastructure.Messaging;
using Trader.Infrastructure.Repositories;
using Trader.Infrastructure.Services;

namespace Trader.Infrastructure;

public static class DependencyInjection
{
    public static IServiceCollection AddInfrastructure(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddMemoryCache();
        services.AddDbContext<TraderContext>(
            options => options.UseNpgsql(configuration.GetConnectionString("DefaultConnection")));
        services.AddRabbitMqMessaging(configuration);
        
        services.AddScoped<IFundRepository, FundRepository>();
        services.AddScoped<IOrderRepository, OrderRepository>();
        services.AddScoped<IPublisher, RabbitMqPublisher>();
        services.AddScoped<ICacheService, MemoryCacheService>();
        
        return services;
    }
}