FROM mcr.microsoft.com/dotnet/aspnet:7.0 AS base
WORKDIR /app
EXPOSE 80

FROM mcr.microsoft.com/dotnet/sdk:7.0 AS build
WORKDIR /src
COPY . .
RUN dotnet restore "Users.API/Users.API.csproj"
RUN dotnet build "Users.API/Users.API.csproj" -c Release -o /app/build

FROM build as publish
RUN dotnet publish "Users.API/Users.API.csproj" -c Release -o /app/publish /p:UseAppHost=false

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
COPY --from=build /src/Users.API/appsettings*.json .
ENTRYPOINT ["dotnet", "Users.API.dll"]
