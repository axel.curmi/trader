using Trader.Application.Dtos;
using Trader.Core.Entities;

namespace Trader.Application.Services;

public interface IFundService
{
    /// <summary>
    /// Obtains all funds available to trade.
    /// </summary>
    /// <returns>ServiceResponseDto containing funds as an IEnumerable.</returns>
    ServiceResponseDto<IEnumerable<Fund>> GetAll();
}