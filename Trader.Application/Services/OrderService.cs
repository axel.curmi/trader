using Commons.Dtos;
using Commons.Messaging;
using Microsoft.Extensions.Logging;
using Trader.Application.Dtos;
using Trader.Core.Entities;
using Trader.Core.Exceptions;
using Trader.Core.Repositories;

namespace Trader.Application.Services;

/// <summary>
/// OrderService is used to perform Order related operations like fetching a user's persisted orders (portfolio)
/// or create new ones.
/// </summary>
public class OrderService : IOrderService
{
    private readonly IOrderRepository _repository;
    private readonly ILogger<OrderService> _logger;
    private readonly IPublisher _publisher;

    public OrderService(IOrderRepository repository, IPublisher publisher, ILogger<OrderService> logger)
    {
        _repository = repository;
        _publisher = publisher;
        _logger = logger;
    }
    
    public ServiceResponseDto<IEnumerable<Order>> GetPortfolio(string email)
    {
        if (email == string.Empty)
        {
            throw new ArgumentException("Email cannot be null.");
        }
        
        _logger.LogDebug("Fetching portfolio for '{email}'", email);
        return new ServiceResponseDto<IEnumerable<Order>>
        {
            Data = _repository.GetOrdersByEmail(email)
        };
    }

    public ServiceResponseDto<Order> DoOrder(string email, TradeRequestDto tradeRequestDto)
    {
        if (email == string.Empty)
        {
            throw new ArgumentException("Email cannot be empty.");
        }
        if (tradeRequestDto.Quantity == 0)
        {
            throw new ArgumentException("Quantity cannot be 0.");
        }
        
        switch (tradeRequestDto.Type)
        {
            case TradeRequestType.Buy:
            {
                return DoBuyOrder(email, tradeRequestDto);
            }
            case TradeRequestType.Sell:
            {
                return DoSellOrder(email, tradeRequestDto);
            }
            default:
            {
                throw new ArgumentException("TradeRequestType can only be 'Buy' or 'Sell'.");
            }
        }
    }

    /// <summary>
    /// Performs the buy order to persist it to DB.
    /// </summary>
    /// <param name="email">The email address of the user to perform the trade order for.</param>
    /// <param name="tradeRequestDto">A DTO containing all data required for the trade (i.e., fundId, quantity, and order type).</param>
    /// <returns>ServiceResponseDto containing the order after being persisted to DB.</returns>
    private ServiceResponseDto<Order> DoBuyOrder(string email, TradeRequestDto tradeRequestDto)
    {
        var response = new ServiceResponseDto<Order>();
        var order = _repository.GetOrderByEmailAndFundId(email, tradeRequestDto.FundId);

        if (order == null)
        {
            _logger.LogInformation("{email} does not have any funds with ID '{id}'; adding new entry in DB.",
                email, tradeRequestDto.FundId);
            response.Data = _repository.AddOrder(new Order()
            {
                Email = email,
                FundId = tradeRequestDto.FundId,
                Quantity = tradeRequestDto.Quantity
            });
        }
        else
        {
            _logger.LogInformation("{email} already owns fund with ID '{id}'; updating existing entry in DB.",
                email, tradeRequestDto.FundId);
            order.Quantity += tradeRequestDto.Quantity;
            response.Data = _repository.UpdateOrder(order);
        }

        _publisher.PublishToActionLog(new TradeOrderActionLogDto
        {
            Email = email,
            FundName = response.Data!.Fund.Name,
            Quantity = tradeRequestDto.Quantity,
            Action = "buy"
        });
        
        return response;
    }
    
    /// <summary>
    /// Performs the sell order to persist it to DB.
    /// </summary>
    /// <param name="email">The email address of the user to perform the trade order for.</param>
    /// <param name="tradeRequestDto">A DTO containing all data required for the trade (i.e., fundId, quantity, and order type).</param>
    /// <returns>ServiceResponseDto containing the order after being persisted to DB.</returns>
    /// <exception cref="OrderExistsException">Thrown if the user attempts to sell a fund that they do not own.</exception>
    /// <exception cref="NotEnoughFundsException">Thrown if the user attempts to sell more funds than he already owns.</exception>
    private ServiceResponseDto<Order> DoSellOrder(string email, TradeRequestDto tradeRequestDto)
    {
        _logger.LogInformation("Performing SellOrder ({email})", email);

        var response = new ServiceResponseDto<Order>();
        var order = _repository.GetOrderByEmailAndFundId(email, tradeRequestDto.FundId);

        if (order == null)
        {
            throw new OrderExistsException(
                $"User '{email}' does not have funds with ID {tradeRequestDto.FundId} in portfolio");
        }

        if (tradeRequestDto.Quantity > order.Quantity)
        {
            throw new NotEnoughFundsException(
                $"User '{email}' does not have enough funds with ID {tradeRequestDto.FundId} to sell {tradeRequestDto.Quantity}");
        }

        if (tradeRequestDto.Quantity == order.Quantity)
        {
            _logger.LogInformation("{email} will not have any funds left with ID '{id}'; deleting entry in DB.",
                email, tradeRequestDto.FundId);
            _repository.DeleteOrder(order);
        }
        else
        {
            _logger.LogInformation("{email} will have remaining funds with ID '{id}'; updating existing entry in DB.",
                email, tradeRequestDto.FundId);
            order.Quantity -= tradeRequestDto.Quantity;
            response.Data = _repository.UpdateOrder(order);
        }
        
        _publisher.PublishToActionLog(new TradeOrderActionLogDto()
        {
            Email = email,
            FundName = order.Fund.Name,
            Quantity = tradeRequestDto.Quantity,
            Action = "sell"
        });

        return response;
    }
}