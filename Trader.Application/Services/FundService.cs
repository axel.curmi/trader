using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Trader.Application.Dtos;
using Trader.Core.Entities;
using Trader.Core.Repositories;

namespace Trader.Application.Services;

/// <summary>
/// FundService is used to perform Fund related operations like fetching all available funds.
/// </summary>
internal class FundService : IFundService
{
    private readonly IFundRepository _fundRepository;
    private readonly ICacheService _cacheService;
    private readonly ILogger<FundService> _logger;

    public FundService(IFundRepository fundRepository, ICacheService cacheService, ILogger<FundService> logger)
    {
        _fundRepository = fundRepository;
        _cacheService = cacheService;
        _logger = logger;
    }
    
    public ServiceResponseDto<IEnumerable<Fund>> GetAll()
    {
        var funds = _cacheService.Get<IEnumerable<Fund>>("allFunds");
        
        // var funds = _memoryCache.Get<IEnumerable<Fund>>("funds");
        if (funds == null)
        {
            _logger.LogInformation("Funds not cached or expired; fetching from DB.");
            funds = _fundRepository.GetAll();
            _cacheService.Set("allFunds", funds, DateTimeOffset.Now.AddMinutes(5));
        }
        else
        {
            _logger.LogInformation("Funds fetched through in memory cache.");
        }
        
        return new ServiceResponseDto<IEnumerable<Fund>>()
        {
            Data = funds
        };
    }
}