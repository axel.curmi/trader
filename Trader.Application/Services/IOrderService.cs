using Trader.Application.Dtos;
using Trader.Core.Entities;

namespace Trader.Application.Services;

public interface IOrderService
{
    /// <summary>
    /// Obtains the portfolio of the user linked to the provided email.
    /// </summary>
    /// <param name="email">The email address of the user to get the portfolio for.</param>
    /// <returns>ServiceResponseDto containing all orders of the user.</returns>
    ServiceResponseDto<IEnumerable<Order>> GetPortfolio(string email);
    
    /// <summary>
    /// Performs an order (can be either a buy or a sell order) to persist it to DB.
    /// </summary>
    /// <param name="email">The email address of the user to perform the trade order for.</param>
    /// <param name="tradeRequestDto">A DTO containing all data required for the trade (i.e., fundId, quantity, and order type).</param>
    /// <returns>ServiceResponseDto containing the order after being persisted to DB.</returns>
    ServiceResponseDto<Order> DoOrder(string email, TradeRequestDto tradeRequestDto);
}