using System.Runtime.CompilerServices;
using Microsoft.Extensions.DependencyInjection;
using Trader.Application.Services;

[assembly: InternalsVisibleTo("Trader.ApplicationTests")]
namespace Trader.Application;

public static class DependencyInjection
{
    public static IServiceCollection AddApplication(this IServiceCollection services)
    {
        services.AddScoped<IFundService, FundService>();
        services.AddScoped<IOrderService, OrderService>();
        return services;
    }
}