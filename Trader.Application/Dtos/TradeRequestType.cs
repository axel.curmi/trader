namespace Trader.Application.Dtos;

public enum TradeRequestType
{
    Buy,
    Sell
}