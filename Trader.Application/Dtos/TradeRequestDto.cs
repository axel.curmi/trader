namespace Trader.Application.Dtos;

public class TradeRequestDto
{
    public TradeRequestType Type { get; set; }
    public Guid FundId { get; set; }
    public uint Quantity { get; set; }
}