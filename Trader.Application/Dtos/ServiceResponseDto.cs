namespace Trader.Application.Dtos;

public class ServiceResponseDto<T>
{
    public string Error { get; set; } = string.Empty;
    public T? Data { get; set; } = default(T);
}