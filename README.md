# Trader

![Architecture diagram](./docs/Architecture.drawio.png)

The solution utilizes a microservice architecture design (currently, there is only one trader microservice) deployed
on the internal network. Clients can access these microservices through an API gateway exposed to the public network.
The microservices perform auditing of relevant actions in two ways: (i) through component logging and (ii) via the
action logger service. The action logger service is a background service that connects with the message broker and
subscribes to the ActionLoggerQueue. When microservices publish messages, these messages are logged and managed by the
action logger to meet audit requirements. Authentication is carried out through Keycloak, where clients must obtain a
token by providing valid credentials and include the bearer token in the Authentication header of their HTTP requests.

Technologies used:
- .NET Core 7.0
- Docker + docker-compose
- Ocelot for API gateway
- Postgres for DB
- RabbitMQ as a message broker
- Keycloak as authentication server
- Python unittesting module for acceptance testing

## Running it locally
Execute `docker-compose up -d` to run everything via Docker + docker-compose and wait a few seconds until everything is up and running.

A custom user for testing purposes has already been created with the following credentials
- Email: dummy.user@test.com
- Password: dummy

However, if you need to create your own custom user, you can follow these instructions to do so from the keycloak administration portal:
    1. Ppen [http://localhost:8080](http://localhost:8080), head over to the administration portal, and login with **admin:admin** credentials.
    2. In the top-left section, change the realm dropdown from master to **trader**, then click on **Users** in the navigation panel on the left.
    3. Click on the **Add user** button and fill in the required details. The required user actions and group inputs should be left empty. Once finished click on the create button.
    4. We should be navigated to the **User details** page. Here we need to click on the **Credentials** tab and then click on the **Set password** button.
    5. Fill in the password and confirmation, **disable** the temporary toggle, and click on save.

## Endpoints
| Service | Verb | Path (API gateway) | Auth required | Caching | Description                                                                                                                                                                               |
| ------- | ---- | ------------------ | ------------- | ------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Users   | POST | /token             | NO            | /       | Responds with a bearer token by providing a valid email and password in the JSON request body. The bearer token needs to be added in the header for requests that require authentication. |
| Trader  | GET  | /trader            | NO            | YES     | Responds with all funds that can be used for trading.                                                                                                                                     |
| Trader  | GET  | /trader/portfolio  | YES           | NO      | Fetches and responds with the currently owned orders of the user.                                                                                                                         |
| Trader  | POST | /trader/trade      | YES           | NO      | Accepts an order trade request containing type (Buy or Sell), fund id, and quantity in the JSON request body and performs the trade operation in DB.                                      |

**Important: A JSON [postman collection](./docs/Trader.postman_collection.json) is available in the docs folder and can be imported to load all requests into postman.**
