FROM mcr.microsoft.com/dotnet/aspnet:7.0 AS base
WORKDIR /app

FROM mcr.microsoft.com/dotnet/sdk:7.0 AS build
WORKDIR /src
COPY . .
RUN dotnet restore "ActionLogger/ActionLogger.csproj"
RUN dotnet build "ActionLogger/ActionLogger.csproj" -c Release -o /app/build

FROM build as publish
RUN dotnet publish "ActionLogger/ActionLogger.csproj" -c Release -o /app/publish /p:UseAppHost=false

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
COPY --from=build /src/ActionLogger/appsettings*.json .
ENTRYPOINT ["dotnet", "ActionLogger.dll"]
