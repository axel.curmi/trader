FROM mcr.microsoft.com/dotnet/aspnet:7.0 AS base
WORKDIR /app
EXPOSE 80
ENV ASPNETCORE_URLS=http://0.0.0.0

FROM mcr.microsoft.com/dotnet/sdk:7.0 AS build
WORKDIR /src
COPY . .
RUN dotnet restore "Trader.API/Trader.API.csproj"
RUN dotnet build "Trader.API/Trader.API.csproj" -c Release -o /app/build

FROM build as publish
RUN dotnet publish "Trader.API/Trader.API.csproj" -c Release -o /app/publish /p:UseAppHost=false

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
COPY --from=build /src/Trader.API/appsettings*.json .
ENTRYPOINT ["dotnet", "Trader.API.dll"]
