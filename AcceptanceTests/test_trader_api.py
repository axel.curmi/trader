import requests
import unittest

from typing import Final

BASE_URL: Final[str] = "http://localhost:8000"

class TestTraderAPI(unittest.TestCase):
    def test_get_all_funds(self):
        response = requests.get(f"{BASE_URL}/trader")

        self.assertEqual(200, response.status_code,
                          "Status code should be 200.")
        
        response_body = response.json()
        self.assertEqual("", response_body["error"],
                         "Response should have no errors.")
        self.assertEqual(8, len(response_body["data"]),
                         "Response should have 8 funds.")

    def test_trade_without_token(self):
        response = requests.post(f"{BASE_URL}/trader/trade", json={
            "type": "Buy",
            "fundId": "0926b072-31a3-41ab-92c8-bea2318343b7",
            "Quantity": 2
        })
        
        self.assertEqual(401, response.status_code,
                         "Status code should be 401.")
        
    def test_portfolio_without_token(self):
        response = requests.get(f"{BASE_URL}/trader/portfolio")
        self.assertEqual(401, response.status_code,
                         "Status code should be 401.")

    def test_buy_order_with_token(self):
        # Get auth token
        response = requests.post(f"{BASE_URL}/token", json={
            "email": "dummy.user@test.com",
            "password": "dummy"
        })
        auth_token = f"Bearer {response.json()['access_token']}"

        # Perform first buy order
        fund_id = "0926b072-31a3-41ab-92c8-bea2318343b7"
        quantity_to_buy = 2

        response = requests.post(f"{BASE_URL}/trader/trade", json={
            "type": "Buy",
            "fundId": fund_id,
            "Quantity": quantity_to_buy
        }, headers={"Authorization": auth_token})
        
        self.assertEqual(200, response.status_code,
                         "Status code should be 200.")
        
        response_body = response.json()
        self.assertEqual("", response_body["error"],
                         "Response should have no errors.")
        self.assertEqual(fund_id, response_body["data"]["fund"]["id"],
                         "Response should have updated fund details.")
        quantity = response_body["data"]["quantity"]

        # Perform second buy order
        response = requests.post(f"{BASE_URL}/trader/trade", json={
            "type": "Buy",
            "fundId": fund_id,
            "Quantity": quantity_to_buy
        }, headers={"Authorization": auth_token})
        response_body = response.json()

        self.assertEqual(quantity + quantity_to_buy, response_body["data"]["quantity"],
                         "Response should have updated quantity.")

    def test_sell_order_with_token(self):
        # Get auth token
        response = requests.post(f"{BASE_URL}/token", json={
            "email": "dummy.user@test.com",
            "password": "dummy"
        })
        auth_token = f"Bearer {response.json()['access_token']}"

        # Perform first buy order
        fund_id = "0926b072-31a3-41ab-92c8-bea2318343b7"
        quantity_to_buy_and_sell = 2

        response = requests.post(f"{BASE_URL}/trader/trade", json={
            "type": "Buy",
            "fundId": fund_id,
            "Quantity": quantity_to_buy_and_sell
        }, headers={"Authorization": auth_token})
        
        response_body = response.json()
        quantity = response_body["data"]["quantity"]

        # Perform second buy order
        response = requests.post(f"{BASE_URL}/trader/trade", json={
            "type": "Sell",
            "fundId": fund_id,
            "Quantity": quantity_to_buy_and_sell
        }, headers={"Authorization": auth_token})
        response_body = response.json()
        
        self.assertEqual(quantity - quantity_to_buy_and_sell, response_body["data"]["quantity"],
                         "Response should have updated quantity.")

if __name__ == "__main__":
    unittest.main()
