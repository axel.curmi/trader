using System.IdentityModel.Tokens.Jwt;
using System.Net.Http.Headers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Trader.Application.Dtos;
using Trader.Application.Services;
using Trader.Core.Entities;
using Trader.Core.Exceptions;

namespace Trader.API.Controllers;

[ApiController]
[Route("/api/trader")]

public class TraderController : ControllerBase
{
    private readonly IFundService _fundService;
    private readonly IOrderService _orderService;
    private readonly ILogger<TraderController> _logger;
    
    private readonly JwtSecurityTokenHandler _tokenHandler;

    public TraderController(IFundService fundService, IOrderService orderService, ILogger<TraderController> logger)
    {
        _fundService = fundService;
        _orderService = orderService;
        _logger = logger;
        
        _tokenHandler = new JwtSecurityTokenHandler();
    }
    
    [HttpGet]
    public ActionResult<IEnumerable<Fund>> GetAllFunds()
    {
        return Ok(_fundService.GetAll());
    }

    [HttpGet("portfolio")]
    [Authorize]
    public ActionResult<IEnumerable<Order>> GetPortfolio()
    {
        var email = GetEmailFromAuthHeader(Request);
        _logger.LogInformation("Fetching orders in portfolio for '{email}'.", email);
        
        return Ok(_orderService.GetPortfolio(email));
    }
    
    [HttpPost("trade")]
    [Authorize]
    public ActionResult<ServiceResponseDto<Order>> Trade([FromBody] TradeRequestDto tradeRequestDto)
    {
        var email = GetEmailFromAuthHeader(Request);
        _logger.LogInformation("User '{email}' attempting to trade ({type}) {quantity} funds with id '{fundId}'",
            email, tradeRequestDto.Type, tradeRequestDto.Quantity, tradeRequestDto.FundId);

        try
        {
            var order = _orderService.DoOrder(email, tradeRequestDto);
            return Ok(order);
        }
        catch (NotEnoughFundsException ex)
        {
            _logger.LogError(ex.Message);
            return BadRequest(new ServiceResponseDto<Order>()
            {
                Error = $"Not enough funds with ID {tradeRequestDto.FundId} to sell {tradeRequestDto.Quantity}."
            });
        }
        catch (OrderExistsException ex)
        {
            _logger.LogError(ex.Message);
            return BadRequest(new ServiceResponseDto<Order>()
            {
                Error = $"No funds with ID {tradeRequestDto.FundId} found in portfolio."
            });
        }
    }

    /// <summary>
    /// Parses the JWT token from the Authentication header in the HTTP request and returns the email address of the user.
    /// </summary>
    /// <param name="request">The HTTP request object</param>
    /// <returns>A string containing the authenticated user's email address.</returns>
    private string GetEmailFromAuthHeader(HttpRequest request)
    {
        var header = AuthenticationHeaderValue.Parse(request.Headers["Authorization"]);
        var token = _tokenHandler.ReadJwtToken(header.Parameter);
        
        return token.Claims.First(claim => claim.Type == "email").Value;
    }
}