using RabbitMQ.Client;

namespace Commons.Messaging;

/// <summary>
/// A RabbitMQ client connecting to the provided server and exposes the channel through a public getter
/// that can be used for publishing and consuming.
/// </summary>
public class RabbitMqClient : IRabbitMqClient, IDisposable
{
    private readonly IConnection _connection;
    public IModel Channel { get; }

    public RabbitMqClient(string connectionUri)
    {
        var connectionFactory = new ConnectionFactory()
        {
            Uri = new Uri(connectionUri)
        };
        _connection = connectionFactory.CreateConnection();
        Channel = _connection.CreateModel();
        
        Channel.ExchangeDeclare(
            exchange: Constants.ActionLoggerExchange, 
            type: "direct", 
            durable: true, 
            autoDelete: false);

        Channel.QueueDeclare(
            queue: Constants.ActionLoggerQueue,
            durable: true,
            exclusive: false,
            autoDelete: false);
        
        Channel.QueueBind(
            queue: Constants.ActionLoggerQueue,
            exchange: Constants.ActionLoggerExchange,
            routingKey: string.Empty);
    }
    
    public void Dispose()
    {
        Channel.Close();
        Channel.Dispose();
        
        _connection.Close();
        _connection.Dispose();
    }
}