namespace Commons.Messaging;

public interface IPublisher
{
    /// <summary>
    /// Publish the value to the provided exchange name.
    /// </summary>
    /// <param name="value">The value to publish.</param>
    /// <param name="exchange">The exchange name to publish on.</param>
    /// <typeparam name="T">The type of the value to publish</typeparam>
    void Publish<T>(T value, string exchange);
    
    /// <summary>
    /// Publish the value to the ActionLog exchange.
    /// This is a replacement for Publish(value, Constants.ActionLogExchange).
    /// </summary>
    /// <param name="value">The value to publish.</param>
    /// <typeparam name="T">The type of the value to publish.</typeparam>
    void PublishToActionLog<T>(T value);
}