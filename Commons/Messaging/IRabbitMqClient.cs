using RabbitMQ.Client;

namespace Commons.Messaging;

public interface IRabbitMqClient
{
    public IModel Channel { get; }
}