namespace Commons.Dtos;

public class TradeOrderActionLogDto
{
    public string Email { get; set; } = string.Empty;
    public string FundName { get; set; }
    public uint Quantity { get; set; }
    public string Action { get; set; }
}