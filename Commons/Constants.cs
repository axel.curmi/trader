namespace Commons;

public static class Constants
{
    public const string ActionLoggerExchange = "ActionLoggerExchange";
    public const string ActionLoggerQueue = "ActionLoggerQueue";
}