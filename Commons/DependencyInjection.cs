using Commons.Messaging;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Commons;

public static class DependencyInjection
{
    public static IServiceCollection AddRabbitMqMessaging(this IServiceCollection services, IConfiguration configuration)
    {
        var connectionUri = configuration.GetSection("Messaging")["RabbitMQ"]
                            ?? throw new ArgumentException(
                                "Missing 'Messaging.RabbitMQ' field in the provided configuration.");
        services.AddSingleton<IRabbitMqClient>(new RabbitMqClient(connectionUri));
        return services;
    }
}