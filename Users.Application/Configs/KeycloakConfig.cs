namespace Users.Application.Configs;

public class KeycloakConfig
{
    public string Url { get; set; } = string.Empty;
    public string Realm { get; set; } = string.Empty;
    public string ClientId { get; set; } = string.Empty;
    public string ClientSecret { get; set; } = string.Empty;

    public bool Valid =>
        Url != string.Empty &&
         Realm != string.Empty &&
         ClientId != string.Empty &&
         ClientSecret != string.Empty;
}