namespace Users.Application.Dtos;

public class GetTokenErrorResponseDto : GetTokenResponseDto
{
    public string Error { get; set; } = string.Empty;
    public string ErrorDescription { get; set; } = string.Empty;
}