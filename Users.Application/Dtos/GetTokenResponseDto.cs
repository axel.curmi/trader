using System.Text.Json.Serialization;

namespace Users.Application.Dtos;

[JsonDerivedType(typeof(GetTokenSuccessfulResponseDto))]
[JsonDerivedType(typeof(GetTokenErrorResponseDto))]
public abstract class GetTokenResponseDto
{
    // Empty
}