using System.Text.Json.Serialization;

namespace Users.Application.Dtos;

public class GetTokenSuccessfulResponseDto : GetTokenResponseDto
{
    [JsonPropertyName("access_token")]
    public string AccessToken { get; set; } = string.Empty;
    
    [JsonPropertyName("token_type")]
    public string TokenType { get; set; } = string.Empty;
}
