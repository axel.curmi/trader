using System.Net;
using System.Text.Json;
using Users.Application.Configs;
using Users.Application.Dtos;
using Users.Core.Exceptions;

namespace Users.Application.Services;

internal class KeycloakTokenService : ITokenService
{
    private readonly KeycloakConfig _keycloakConfig;

    public KeycloakTokenService(KeycloakConfig keycloakConfig)
    {
        if (!keycloakConfig.Valid)
        {
            throw new ArgumentException("Keycloak configs provided are invalid. Make sure all fields are populated.");
        }
        _keycloakConfig = keycloakConfig;
    }
    
    public async Task<GetTokenResponseDto> GetToken(GetTokenRequestDto dto)
    {
        if (dto.Email == string.Empty || dto.Password == string.Empty)
        {
            throw new ArgumentException("Fields email and password cannot be empty.");
        }
        
        var formData = new Dictionary<string, string>
        {
            { "grant_type", "password" },
            { "client_id", _keycloakConfig.ClientId },
            { "client_secret", _keycloakConfig.ClientSecret },
            { "username", dto.Email },
            { "password", dto.Password }
        };

        var getTokenUrl = $"{_keycloakConfig.Url}/realms/{_keycloakConfig.Realm}/protocol/openid-connect/token";
  
        var httpClient = new HttpClient();
        var response = await httpClient.PostAsync(getTokenUrl,
            new FormUrlEncodedContent(formData));
        var responseContent = await response.Content.ReadAsStringAsync();
        
        if (response.IsSuccessStatusCode)
        {
            return JsonSerializer.Deserialize<GetTokenSuccessfulResponseDto>(responseContent)!;
        }

        if (response.StatusCode == HttpStatusCode.Unauthorized)
        {
            return JsonSerializer.Deserialize<GetTokenErrorResponseDto>(responseContent)!;
        }

        throw new FetchTokenException(
            $"An error occurred when attempting to fetch token (status:{response.StatusCode}) : {responseContent}");
    }
}