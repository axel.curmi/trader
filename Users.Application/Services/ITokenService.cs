using Users.Application.Dtos;

namespace Users.Application.Services;

public interface ITokenService
{ 
    /// <summary>
    /// Communicates with the authentication server and requests an access token that will be required by other services.
    /// </summary>
    /// <param name="dto">A GetTokenRequestDto containing the username and password</param>
    /// <returns>A GetTokenResponseDto which can either be GetTokenSuccessfulResponseDto or GetTokenErrorResponseDto containing corresponding response data.</returns>
    Task<GetTokenResponseDto> GetToken(GetTokenRequestDto dto);
}