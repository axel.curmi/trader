using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Users.Application.Configs;
using Users.Application.Services;

namespace Users.Application;

public static class DependencyInjection
{
    public static IServiceCollection AddApplication(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddScoped<ITokenService>(s => new KeycloakTokenService(
            new KeycloakConfig
            {
                Url = configuration.GetSection("Keycloak")["auth-server-url"]!,
                Realm = configuration.GetSection("Keycloak")["realm"]!,
                ClientId = configuration.GetSection("Keycloak")["client-id"]!,
                ClientSecret = configuration.GetSection("Keycloak")["client-secret"]!
            }));

        return services;
    }
}