using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Moq;
using Trader.Application.Dtos;
using Trader.Application.Services;
using Trader.Core.Entities;
using Trader.Core.Enums;
using Trader.Core.Enums.Equity;
using Trader.Core.Repositories;

namespace Trader.ApplicationTests.Services;

public class FundServiceTests
{
    private IFundService _fundService = null!;
    private Mock<IFundRepository> _repository = null!;
    private Mock<ICacheService> _cacheService = null!;
    
    [SetUp]
    public void Setup()
    {
        var logger = new LoggerFactory().CreateLogger<FundService>();
        _repository = new Mock<IFundRepository>();
        _cacheService = new Mock<ICacheService>();
        
        _fundService = new FundService(_repository.Object, _cacheService.Object, logger);
    }
    
    [Test]
    public void GetAll_Valid()
    {
        var funds = new Fund[]
        {
            new Equity()
            {
                Id = Guid.NewGuid(),
                Name = "TestEquity",
                Currency = Currency.Euro,
                Sector = Sector.Energy,
                Value = 9.99
            },
            new MutualFund
            {
                Id = Guid.NewGuid(),
                Name = "TestMutualFund",
                Currency = Currency.PoundSterling,
                RiskRating = 5,
                NetAssetValue = 135.00,
                DistributionStatus = DistributionStatus.Accumulating,
            }
        };
        var expectedResult = new ServiceResponseDto<IEnumerable<Fund>>
        {
            Error = string.Empty,
            Data = funds
        };
        _cacheService.Setup(c => c.Get<IEnumerable<Fund>>(
            It.IsAny<string>())).Returns((IEnumerable<Fund>?)null);
        _repository.Setup(r => r.GetAll()).Returns(funds);
        
        var actualResult = _fundService.GetAll();
        
        Assert.Multiple(() =>
        {
            Assert.That(actualResult, Is.InstanceOf<ServiceResponseDto<IEnumerable<Fund>>>(),
                "Return is not ServiceResponseDto<IEnumerable<Fund>>.");
            Assert.That(actualResult.Error, Is.EqualTo(string.Empty),
                    "Return should not contain any errors.");
            Assert.That(actualResult.Data, Is.EqualTo(expectedResult.Data),
                "Return data is not as expected.");
        });
    }

    [Test]
    public void GetAll_EmptyCache_ShouldCallDbAndSetCache()
    {
        var funds = new Fund[]
        {
            new Equity()
            {
                Id = Guid.NewGuid(),
                Name = "TestEquity",
                Currency = Currency.Euro,
                Sector = Sector.Energy,
                Value = 9.99
            },
            new MutualFund
            {
                Id = Guid.NewGuid(),
                Name = "TestMutualFund",
                Currency = Currency.PoundSterling,
                RiskRating = 5,
                NetAssetValue = 135.00,
                DistributionStatus = DistributionStatus.Accumulating,
            }
        };
        _repository.Setup(r => r.GetAll()).Returns(funds);
        _cacheService.Setup(c => c.Get<IEnumerable<Fund>>(
            It.IsAny<string>())).Returns((IEnumerable<Fund>?)null);
        
        _fundService.GetAll();
        
        _cacheService.Verify(c => c.Get<IEnumerable<Fund>>("allFunds"), Times.Exactly(1));
        _repository.Verify(r => r.GetAll(), Times.Once);
        _cacheService.Verify(c => c.Set(
            "allFunds", It.IsAny<IEnumerable<Fund>>(), It.IsAny<DateTimeOffset>()), Times.Once);
    }

    [Test]
    public void GetAll_FromCache_ShouldNotCallDb()
    {
        var funds = new Fund[]
        {
            new Equity()
            {
                Id = Guid.NewGuid(),
                Name = "TestEquity",
                Currency = Currency.Euro,
                Sector = Sector.Energy,
                Value = 9.99
            },
            new MutualFund
            {
                Id = Guid.NewGuid(),
                Name = "TestMutualFund",
                Currency = Currency.PoundSterling,
                RiskRating = 5,
                NetAssetValue = 135.00,
                DistributionStatus = DistributionStatus.Accumulating,
            }
        };
        _cacheService.Setup(c => c.Get<IEnumerable<Fund>>(
            It.IsAny<string>())).Returns(funds);
        
        _fundService.GetAll();
        
        _cacheService.Verify(c => c.Get<IEnumerable<Fund>>("allFunds"), Times.Exactly(1));
        _repository.Verify(r => r.GetAll(), Times.Never);
        _cacheService.Verify(c => c.Set(
            "allFunds", It.IsAny<IEnumerable<Fund>>(), It.IsAny<DateTimeOffset>()), Times.Never);
    }
}