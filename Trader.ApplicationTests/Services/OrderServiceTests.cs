using Commons.Dtos;
using Commons.Messaging;
using Microsoft.Extensions.Logging;
using Moq;
using Trader.Application.Dtos;
using Trader.Application.Services;
using Trader.Core.Entities;
using Trader.Core.Exceptions;
using Trader.Core.Repositories;

namespace Trader.ApplicationTests.Services;

public class OrderServiceTests
{
    private Mock<IOrderRepository> _repository = null!;
    private Mock<IPublisher> _publisher = null!;
    private IOrderService _orderService = null!;

    [SetUp]
    public void Setup()
    {
        _repository = new Mock<IOrderRepository>();
        _publisher = new Mock<IPublisher>();
        
        var logger = new LoggerFactory().CreateLogger<OrderService>();
        _orderService = new OrderService(_repository.Object, _publisher.Object, logger);
    }

    [Test]
    public void GetPortfolio_EmptyEmail()
    {
        Assert.Throws<ArgumentException>(delegate
        {
            _orderService.GetPortfolio(string.Empty);
        });
    }

    [Test]
    public void GetPortfolio_ValidEmail()
    {
        var orders = new Order[]
        {
            new()
            {
                Email = "test@test.com",
                FundId = Guid.NewGuid(),
                Fund = null!,
                Quantity = 2
            },
            new()
            {
                Email = "test@test.com",
                FundId = Guid.NewGuid(),
                Fund = null!,
                Quantity = 3
            },
        };
        var expectedResult = new ServiceResponseDto<IEnumerable<Order>>
        {
            Error = string.Empty,
            Data = orders
        };
        _repository.Setup(r => r.GetOrdersByEmail(It.IsAny<string>()))
            .Returns(orders);

        var actualResult = _orderService.GetPortfolio("test@test.com");
        
        Assert.IsInstanceOf<ServiceResponseDto<IEnumerable<Order>>>(actualResult,
            "Return is not ServiceResponseDto<IEnumerable<Order>>.");
        Assert.AreEqual(string.Empty, actualResult.Error,
            "Return should not contain any errors.");
        Assert.AreEqual(expectedResult.Data, actualResult.Data,
            "Return data is not as expected.");
    }

    [Test]
    public void DoOrder_ZeroQuantity_ShouldThrowArgumentException()
    {
        var tradeRequestDto = new TradeRequestDto
        {
            Type = TradeRequestType.Buy,
            FundId = Guid.NewGuid(),
            Quantity = 0
        };

        Assert.Throws<ArgumentException>(delegate
        {
            _orderService.DoOrder("test@test.com", tradeRequestDto);
        });
    }
    
    [Test]
    public void DoOrder_EmptyEmail_ShouldThrowArgumentException()
    {
        var tradeRequestDto = new TradeRequestDto
        {
            Type = TradeRequestType.Buy,
            FundId = Guid.NewGuid(),
            Quantity = 1
        };

        Assert.Throws<ArgumentException>(delegate
        {
            _orderService.DoOrder(string.Empty, tradeRequestDto);
        });
    }
    
    [Test]
    public void DoOrder_Buy_FirstTime()
    {
        var tradeRequestDto = new TradeRequestDto
        {
            Type = TradeRequestType.Buy,
            FundId = Guid.NewGuid(),
            Quantity = 2
        };
        _repository.Setup(r => r.GetOrderByEmailAndFundId(
            It.IsAny<string>(), It.IsAny<Guid>())).Returns((Order?)null);
        _repository.Setup(r => r.AddOrder(It.IsAny<Order>()))
            .Returns(new Order { Fund = new MutualFund { Name = "Some MutualFund" } });

        _orderService.DoOrder("test@test.com", tradeRequestDto);
        
        _repository.Verify(r => r.AddOrder(It.IsAny<Order>()), Times.Once);
        _publisher.Verify(p => p.PublishToActionLog(It.IsAny<TradeOrderActionLogDto>()), Times.Once);
    }

    [Test]
    public void DoOrder_Buy_ExistingOrder()
    {
        var tradeRequestDto = new TradeRequestDto
        {
            Type = TradeRequestType.Buy,
            FundId = Guid.NewGuid(),
            Quantity = 2
        };
        var order = new Order
        {
            Email = "test@test.com",
            FundId = Guid.NewGuid(),
            Fund = null!,
            Quantity = 1
        };
        
        _repository.Setup(r => r.GetOrderByEmailAndFundId(
            It.IsAny<string>(), It.IsAny<Guid>())).Returns(order);
        _repository.Setup(r => r.UpdateOrder(It.IsAny<Order>()))
            .Returns(new Order { Fund = new MutualFund { Name = "Some Mutual Fund"} });
        
        _orderService.DoOrder("test@test.com", tradeRequestDto);
        
        Assert.AreEqual(3, order.Quantity, "Quantity of the order should be 3.");
        _repository.Verify(r => r.UpdateOrder(order), Times.Once);
        _publisher.Verify(p => p.PublishToActionLog(It.IsAny<TradeOrderActionLogDto>()), Times.Once);
    }

    [Test]
    public void DoOrder_Sell_NullOrder_ShouldThrowOrderExistsException()
    {
        var tradeRequestDto = new TradeRequestDto
        {
            Type = TradeRequestType.Sell,
            FundId = Guid.NewGuid(),
            Quantity = 2
        };
        _repository.Setup(r => r.GetOrderByEmailAndFundId(
            It.IsAny<string>(), It.IsAny<Guid>())).Returns((Order?)null);

        Assert.Throws<OrderExistsException>(delegate
        {
            _orderService.DoOrder("test@test.com", tradeRequestDto);
        });
    }
    
    [Test]
    public void DoOrder_Sell_SellMoreThanOwned_ShouldThrowNotEnoughFundsException()
    {
        var tradeRequestDto = new TradeRequestDto
        {
            Type = TradeRequestType.Sell,
            FundId = Guid.NewGuid(),
            Quantity = 10
        };
        var order = new Order
        {
            Email = "test@test.com",
            FundId = Guid.NewGuid(),
            Fund = null!,
            Quantity = 5
        };
        
        _repository.Setup(r => r.GetOrderByEmailAndFundId(
            It.IsAny<string>(), It.IsAny<Guid>())).Returns(order);

        Assert.Throws<NotEnoughFundsException>(delegate
        {
            _orderService.DoOrder("test@test.com", tradeRequestDto);
        });
    }
    
    [Test]
    public void DoOrder_Sell_SellLessThanOwned_ShouldUpdateExisting()
    {
        var tradeRequestDto = new TradeRequestDto
        {
            Type = TradeRequestType.Sell,
            FundId = Guid.NewGuid(),
            Quantity = 2
        };
        var order = new Order
        {
            Email = "test@test.com",
            FundId = Guid.NewGuid(),
            Fund = new MutualFund
            {
                Name = "Some MutualFund"
            },
            Quantity = 5
        };
        
        _repository.Setup(r => r.GetOrderByEmailAndFundId(
            It.IsAny<string>(), It.IsAny<Guid>())).Returns(order);

        _orderService.DoOrder("test@test.com", tradeRequestDto);
        
        Assert.AreEqual(3, order.Quantity, "Quantity of the order should be 3.");
        _repository.Verify(r => r.UpdateOrder(order), Times.Once);
        _publisher.Verify(p => p.PublishToActionLog(It.IsAny<TradeOrderActionLogDto>()), Times.Once);
    }
    
    [Test]
    public void DoOrder_Sell_SellAllOwned_ShouldUpdateExisting()
    {
        var tradeRequestDto = new TradeRequestDto
        {
            Type = TradeRequestType.Sell,
            FundId = Guid.NewGuid(),
            Quantity = 2
        };
        var order = new Order
        {
            Email = "test@test.com",
            FundId = Guid.NewGuid(),
            Fund = new MutualFund
            {
                Name = "Some MutualFund"
            },
            Quantity = 2
        };
        
        _repository.Setup(r => r.GetOrderByEmailAndFundId(
            It.IsAny<string>(), It.IsAny<Guid>())).Returns(order);

        _orderService.DoOrder("test@test.com", tradeRequestDto);
        
        _repository.Verify(r => r.DeleteOrder(order), Times.Once);
        _publisher.Verify(p => p.PublishToActionLog(It.IsAny<TradeOrderActionLogDto>()), Times.Once);
    }
}